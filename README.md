# `emuriscv`

`emuriscv` is an emulator of the RV32IMC (RISC-V) ISA.


## Installation


You can install the latest version from the git repository:

```bash
cargo install --git https://gitlab.com/dzamlo/emuriscv.git
```

If you use Arch Linux, a `PKGBUILD` file is present in the `aur` directory.

bash, zsh and fish completion files are also generated but not installed with `cargo install`. To use them you need to either use the `PKGBUILD` file, or manually build the application yourself and copy the files.

## Usage

To create examples executables, go into the `executables` folder and run `make`.

You can then go into the root folder and run one of the executables like that: `cargo run -- --debug -e executables/not.bin`

## Syscall

Here is a table of the syscall can call using the `ecall` instruction (see `executables/cat.s` or `executables/hello_world.s` for examples):

name | syscall number (a7) | a0 | a1 | a2 | a3 | a4 | a5 | a6 |
-----|---------------------|----|----|----|----|----|----|----|
exit|93|exit code |-|-|-|-|-|-|
read|63|fd|*buf| count|-|-|-|-|
write|64|fd|*buf| count|-|-|-|-|
open|512|*path (zero terminated)|flags|-|-|-|-|-|
get_random|1024|-|-|-|-|-|-|-|

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
