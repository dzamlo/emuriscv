.global _start
_start:
    la a1, .buf
    li t0, -1
loop:
    li a7, 63 # syscall 63 = read
    li a0, 0 # fd = stdin
    li a2, 10
    ecall
    beqz a0, exit
    beq a0, t0, exit
    li a7, 64 # syscall 64 = write
    mv a2, a0
    li a0, 1 # fd = stdout
    ecall 
    j loop
exit:
    addi a7, zero, 93 # syscall 93 = exit
    li a0, 0
    ecall

.buf:
    .ascii "aaaaaaaaaa" # will be used as a 10 bytes buffer
