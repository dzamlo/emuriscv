.global _start
_start:
    li x8, 42
    li x9, 4
    mulh x11, x9, x8
    mul x10, x9, x8
    div x10, x8, x9
    rem x11, x8, x9
    # exit:
    li a7, 93 # syscall 93 = exit
    li a0, 0
    ecall
