.global _start

_start:
    li a7, 512 # syscall 512 = open
    la a0, .path
    li a1, 0 # flags = 0
    ecall # at return of the syscall, a0=fd
    li a7, 63 # syscall 63 = read
    li a1, 1000
    li a2, 1
    ecall
    lb a0, 1000(zero) # load the byte we read in a0
    addi a7, zero, 93 # syscall 93 = exit
    ecall

.path:
    .ascii "/dev/urandom\0"
