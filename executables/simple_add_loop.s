.global _start
_start:
    li x8, 10
    li x9, 20
    add x10, x10, x8
    add x10, x10, x9
    srli x10,x10, 1
    j _start # pseudo instruction for jal x0, _foo

