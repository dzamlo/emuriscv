use std::fmt::Debug;
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::BitAnd;
use std::ops::Shr;

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Address(pub u32);

impl Debug for Address {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "0x{:08X}", self.0)
    }
}

impl From<u32> for Address {
    fn from(value: u32) -> Self {
        Address(value)
    }
}

impl Add<u32> for Address {
    type Output = Address;

    fn add(self, rhs: u32) -> Self::Output {
        Address(self.0 + rhs)
    }
}

impl AddAssign<u32> for Address {
    fn add_assign(&mut self, rhs: u32) {
        *self = *self + rhs;
    }
}

impl BitAnd<u32> for Address {
    type Output = Address;

    fn bitand(self, rhs: u32) -> Self::Output {
        Address(self.0 & rhs)
    }
}

impl Shr<u32> for Address {
    type Output = Address;

    fn shr(self, rhs: u32) -> Self::Output {
        Address(self.0 >> rhs)
    }
}

impl PartialEq<u32> for Address {
    fn eq(&self, other: &u32) -> bool {
        self.0 == *other
    }
}

impl PartialOrd<u32> for Address {
    fn partial_cmp(&self, other: &u32) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(other)
    }
}

impl Address {
    pub const ZERO: Address = Address(0);
}
