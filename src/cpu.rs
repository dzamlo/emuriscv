use crate::address::Address;
use crate::bus::Bus;
use crate::ecall;
use crate::ecall::EcallHandler;
use anyhow::bail;
use anyhow::Result;

type RegisterAddress = u8;
const REGISTER_ADDRESS_MASK: u8 = 0x1F;
const COMPRESSED_QUADRANT_MASK: u8 = 0b11;
const RD_SHIFT: u8 = 7;
const RS1_SHIFT: u8 = 15;
const RS2_SHIFT: u8 = 20;
const FUNCT3_MASK: u8 = 0x7;
const FUNCT3_SHIFT: u8 = 12;
const COMPRESSED_FUNCT3_SHIFT: u8 = 13;
const FUNCT7_SHIFT: u8 = 25;
const FUNCT7_MASK: u8 = 0x7F;
const OPCODE_MASK: u8 = 0x7F;
const SHAMT_MASK: u8 = 0x1F;
const A0_REGISTER_ADDRESS: RegisterAddress = 10;
const A7_REGISTER_ADDRESS: RegisterAddress = 17;

#[derive(Clone, Debug)]
pub struct Cpu {
    registers: [u32; 32],
    pc: Address,
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            registers: [0; 32],
            pc: Address::ZERO,
        }
    }

    pub fn execute<B: Bus, E: EcallHandler>(
        &mut self,
        instruction: Instruction,
        bus: &mut B,
        ecall_handler: &mut E,
        debug: bool,
    ) -> Result<()> {
        if debug {
            eprintln!("Executing instruction {instruction:?} on {self:?}");
        }

        let mut inc_pc = true;

        match instruction.decompressed_instruction {
            DecompressedInstruction::Lui { dest, immediate } => {
                self.write_register(dest, immediate, debug)
            }
            DecompressedInstruction::Auipc { dest, immediate } => {
                self.write_register(dest, self.pc.0.wrapping_add_signed(immediate), debug)
            }
            DecompressedInstruction::Jal { dest, offset } => {
                inc_pc = false;
                self.write_register(dest, self.pc.0.wrapping_add(4), debug);
                self.jump_relative(offset, debug)?;
            }
            DecompressedInstruction::Jalr { dest, base, offset } => {
                inc_pc = false;
                self.write_register(dest, self.pc.0.wrapping_add(4), debug);
                self.jump(self.compute_address(base, offset, debug), debug)?;
            }
            DecompressedInstruction::Beq { src1, src2, offset } => {
                if self.read_register(src1, debug) == self.read_register(src2, debug) {
                    if debug {
                        eprintln!("Branch BEQ taken");
                    }
                    inc_pc = false;
                    self.jump_relative(offset, debug)?;
                } else if debug {
                    eprintln!("Branch BEQ not taken");
                }
            }
            DecompressedInstruction::Bne { src1, src2, offset } => {
                if self.read_register(src1, debug) != self.read_register(src2, debug) {
                    if debug {
                        eprintln!("Branch BNE taken");
                    }
                    inc_pc = false;
                    self.jump_relative(offset, debug)?;
                } else if debug {
                    eprintln!("Branch BNE not taken");
                }
            }
            DecompressedInstruction::Blt { src1, src2, offset } => {
                if (self.read_register(src1, debug) as i32)
                    < (self.read_register(src2, debug) as i32)
                {
                    if debug {
                        eprintln!("Branch BLT taken");
                    }
                    inc_pc = false;
                    self.jump_relative(offset, debug)?;
                } else if debug {
                    eprintln!("Branch BLT not taken");
                }
            }
            DecompressedInstruction::Bge { src1, src2, offset } => {
                if (self.read_register(src1, debug) as i32)
                    >= (self.read_register(src2, debug) as i32)
                {
                    if debug {
                        eprintln!("Branch BGE taken");
                    }
                    inc_pc = false;
                    self.jump_relative(offset, debug)?;
                } else if debug {
                    eprintln!("Branch BGE not taken");
                }
            }
            DecompressedInstruction::Bltu { src1, src2, offset } => {
                if self.read_register(src1, debug) < self.read_register(src2, debug) {
                    if debug {
                        eprintln!("Branch BLTU taken");
                    }
                    inc_pc = false;
                    self.jump_relative(offset, debug)?;
                } else if debug {
                    eprintln!("Branch BLTU not taken");
                }
            }
            DecompressedInstruction::Bgeu { src1, src2, offset } => {
                if self.read_register(src1, debug) >= self.read_register(src2, debug) {
                    if debug {
                        eprintln!("Branch BGEU taken");
                    }
                    inc_pc = false;
                    self.jump_relative(offset, debug)?;
                } else if debug {
                    eprintln!("Branch BGEU not taken");
                }
            }
            DecompressedInstruction::Lb { dest, base, offset } => {
                let value = bus.read_u8(self.compute_address(base, offset, debug), debug)?;
                let value = ((value as i32) << 24) >> 24;
                self.write_register(dest, value as u32, debug)
            }
            DecompressedInstruction::Lh { dest, base, offset } => {
                let value = bus.read_u16(self.compute_address(base, offset, debug), debug)?;
                let value = ((value as i32) << 16) >> 16;
                self.write_register(dest, value as u32, debug)
            }
            DecompressedInstruction::Lw { dest, base, offset } => {
                let value = bus.read_u32(self.compute_address(base, offset, debug), debug)?;
                self.write_register(dest, value, debug)
            }
            DecompressedInstruction::Lbu { dest, base, offset } => {
                let value = bus.read_u8(self.compute_address(base, offset, debug), debug)?;
                self.write_register(dest, value as u32, debug)
            }
            DecompressedInstruction::Lhu { dest, base, offset } => {
                let value = bus.read_u16(self.compute_address(base, offset, debug), debug)?;
                self.write_register(dest, value as u32, debug)
            }
            DecompressedInstruction::Sb { src, base, offset } => bus.write_u8(
                self.compute_address(base, offset, debug),
                self.read_register(src, debug) as u8,
                debug,
            )?,
            DecompressedInstruction::Sh { src, base, offset } => bus.write_u16(
                self.compute_address(base, offset, debug),
                self.read_register(src, debug) as u16,
                debug,
            )?,
            DecompressedInstruction::Sw { src, base, offset } => bus.write_u32(
                self.compute_address(base, offset, debug),
                self.read_register(src, debug),
                debug,
            )?,
            DecompressedInstruction::Addi {
                dest,
                src,
                immediate,
            } => self.write_register(
                dest,
                self.read_register(src, debug)
                    .wrapping_add_signed(immediate),
                debug,
            ),
            DecompressedInstruction::Slti {
                dest,
                src,
                immediate,
            } => {
                if (self.read_register(src, debug) as i32) < immediate {
                    self.write_register(dest, 1, debug)
                } else {
                    self.write_register(dest, 0, debug)
                }
            }
            DecompressedInstruction::Sltiu {
                dest,
                src,
                immediate,
            } => {
                if self.read_register(src, debug) < (immediate as u32) {
                    self.write_register(dest, 1, debug)
                } else {
                    self.write_register(dest, 0, debug)
                }
            }
            DecompressedInstruction::Xori {
                dest,
                src,
                immediate,
            } => self.write_register(
                dest,
                self.read_register(src, debug) ^ (immediate as u32),
                debug,
            ),
            DecompressedInstruction::Ori {
                dest,
                src,
                immediate,
            } => self.write_register(
                dest,
                self.read_register(src, debug) | (immediate as u32),
                debug,
            ),
            DecompressedInstruction::Andi {
                dest,
                src,
                immediate,
            } => self.write_register(
                dest,
                self.read_register(src, debug) & (immediate as u32),
                debug,
            ),
            DecompressedInstruction::Slli { dest, src, shamt } => {
                self.write_register(dest, self.read_register(src, debug) << shamt, debug)
            }

            DecompressedInstruction::Srli { dest, src, shamt } => {
                self.write_register(dest, self.read_register(src, debug) >> shamt, debug)
            }
            DecompressedInstruction::Srai { dest, src, shamt } => self.write_register(
                dest,
                ((self.read_register(src, debug) as i32) >> shamt) as u32,
                debug,
            ),
            DecompressedInstruction::Add { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug)
                    .wrapping_add(self.read_register(src2, debug)),
                debug,
            ),
            DecompressedInstruction::Sub { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug)
                    .wrapping_sub(self.read_register(src2, debug)),
                debug,
            ),
            DecompressedInstruction::Sll { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug)
                    << (self.read_register(src2, debug) as u8 & SHAMT_MASK),
                debug,
            ),
            DecompressedInstruction::Slt { dest, src1, src2 } => {
                if (self.read_register(src1, debug) as i32)
                    < (self.read_register(src2, debug) as i32)
                {
                    self.write_register(dest, 1, debug)
                } else {
                    self.write_register(dest, 0, debug)
                }
            }
            DecompressedInstruction::Sltu { dest, src1, src2 } => {
                if self.read_register(src1, debug) < self.read_register(src2, debug) {
                    self.write_register(dest, 1, debug)
                } else {
                    self.write_register(dest, 0, debug)
                }
            }
            DecompressedInstruction::Xor { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug) ^ self.read_register(src2, debug),
                debug,
            ),
            DecompressedInstruction::Srl { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug)
                    >> (self.read_register(src2, debug) as u8 & SHAMT_MASK),
                debug,
            ),
            DecompressedInstruction::Sra { dest, src1, src2 } => self.write_register(
                dest,
                (self.read_register(src1, debug) as i32
                    >> (self.read_register(src2, debug) as u8 & SHAMT_MASK)) as u32,
                debug,
            ),
            DecompressedInstruction::Or { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug) | self.read_register(src2, debug),
                debug,
            ),
            DecompressedInstruction::And { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug) & self.read_register(src2, debug),
                debug,
            ),
            DecompressedInstruction::Fence { .. } => (), // Ignore this instruction
            DecompressedInstruction::Ecall => self.write_register(
                A0_REGISTER_ADDRESS,
                ecall_handler.handle_ecall(
                    self.read_register(A7_REGISTER_ADDRESS, debug),
                    self.registers[A0_REGISTER_ADDRESS as usize
                        ..(A0_REGISTER_ADDRESS as usize + ecall::ARGS_LEN)]
                        .try_into()
                        .unwrap(),
                    bus,
                    debug,
                )?,
                debug,
            ),
            DecompressedInstruction::Ebreak => todo!(),
            DecompressedInstruction::Mul { dest, src1, src2 } => self.write_register(
                dest,
                self.read_register(src1, debug)
                    .wrapping_mul(self.read_register(src2, debug)),
                debug,
            ),
            DecompressedInstruction::Mulh { dest, src1, src2 } => self.write_register(
                dest,
                ((self.read_register(src1, debug) as i64)
                    .wrapping_mul(self.read_register(src2, debug) as i64)
                    >> 32) as u32,
                debug,
            ),
            DecompressedInstruction::Mulhsu { dest, src1, src2 } => self.write_register(
                dest,
                ((self.read_register(src1, debug) as i128 as u128)
                    .wrapping_mul(self.read_register(src2, debug) as u128)
                    >> 32) as u32,
                debug,
            ),
            DecompressedInstruction::Mulhu { dest, src1, src2 } => self.write_register(
                dest,
                ((self.read_register(src1, debug) as u64)
                    .wrapping_mul(self.read_register(src2, debug) as u64)
                    >> 32) as u32,
                debug,
            ),
            DecompressedInstruction::Div { dest, src1, src2 } => {
                let src1 = self.read_register(src1, debug) as i32;
                let src2 = self.read_register(src2, debug) as i32;
                let result = if src2 == 0 { -1i32 } else { src1 / src2 };
                self.write_register(dest, result as u32, debug);
            }
            DecompressedInstruction::Divu { dest, src1, src2 } => {
                let src1 = self.read_register(src1, debug);
                let src2 = self.read_register(src2, debug);
                let result = if src2 == 0 { u32::MAX } else { src1 / src2 };
                self.write_register(dest, result, debug);
            }
            DecompressedInstruction::Rem { dest, src1, src2 } => {
                let src1 = self.read_register(src1, debug) as i32;
                let src2 = self.read_register(src2, debug) as i32;
                let result = if src2 == 0 { src1 } else { src1 % src2 };
                self.write_register(dest, result as u32, debug);
            }
            DecompressedInstruction::Remu { dest, src1, src2 } => {
                let src1 = self.read_register(src1, debug);
                let src2 = self.read_register(src2, debug);
                let result = if src2 == 0 { src1 } else { src1 % src2 };
                self.write_register(dest, result, debug);
            }
            DecompressedInstruction::IllegalInstruction(value) => {
                bail!("Illegal instruction (0x{value:X})")
            }
        }
        if inc_pc {
            if debug {
                eprintln!("Incrementing pc")
            }
            if instruction.compressed {
                self.pc += 2;
            } else {
                self.pc += 4;
            }
        } else if debug {
            eprintln!("Not incrementing pc")
        }
        Ok(())
    }

    pub fn get_pc(&self) -> Address {
        self.pc
    }

    pub fn jump(&mut self, address: Address, debug: bool) -> Result<()> {
        if debug {
            eprintln!("Jumping to address {address:?}");
        }

        self.pc = address;
        Ok(())
    }
    pub fn jump_relative(&mut self, offset: i32, debug: bool) -> Result<()> {
        self.jump(self.pc.0.wrapping_add_signed(offset).into(), debug)
    }

    pub fn write_register(&mut self, address: RegisterAddress, value: u32, debug: bool) {
        // Ignore write to register 0, this register must stay at value 0
        if address > 0 {
            if debug {
                eprintln!("Writing to register {address} (value={value})");
            }
            self.registers[address as usize] = value;
        } else if debug {
            eprintln!("Ignoring write to register 0 (value={value})");
        }
    }

    pub fn read_register(&self, address: RegisterAddress, debug: bool) -> u32 {
        let value = self.registers[address as usize];
        if debug {
            eprintln!("Reading register {address} (value={value})");
        }

        value
    }

    fn compute_address(&self, base: RegisterAddress, offset: i32, debug: bool) -> Address {
        ((self.read_register(base, debug) as i64 + offset as i64) as u32).into()
    }
}

impl Default for Cpu {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Copy, Clone, Debug)]
pub enum DecompressedInstruction {
    Lui {
        dest: RegisterAddress,
        immediate: u32,
    },
    Auipc {
        dest: RegisterAddress,
        immediate: i32,
    },
    Jal {
        dest: RegisterAddress,
        offset: i32,
    },
    Jalr {
        dest: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Beq {
        src1: RegisterAddress,
        src2: RegisterAddress,
        offset: i32,
    },
    Bne {
        src1: RegisterAddress,
        src2: RegisterAddress,
        offset: i32,
    },
    Blt {
        src1: RegisterAddress,
        src2: RegisterAddress,
        offset: i32,
    },
    Bge {
        src1: RegisterAddress,
        src2: RegisterAddress,
        offset: i32,
    },
    Bltu {
        src1: RegisterAddress,
        src2: RegisterAddress,
        offset: i32,
    },
    Bgeu {
        src1: RegisterAddress,
        src2: RegisterAddress,
        offset: i32,
    },
    Lb {
        dest: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Lh {
        dest: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Lw {
        dest: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Lbu {
        dest: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Lhu {
        dest: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Sb {
        src: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Sh {
        src: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Sw {
        src: RegisterAddress,
        base: RegisterAddress,
        offset: i32,
    },
    Addi {
        dest: RegisterAddress,
        src: RegisterAddress,
        immediate: i32,
    },
    Slti {
        dest: RegisterAddress,
        src: RegisterAddress,
        immediate: i32,
    },
    Sltiu {
        dest: RegisterAddress,
        src: RegisterAddress,
        immediate: i32,
    },
    Xori {
        dest: RegisterAddress,
        src: RegisterAddress,
        immediate: i32,
    },
    Ori {
        dest: RegisterAddress,
        src: RegisterAddress,
        immediate: i32,
    },
    Andi {
        dest: RegisterAddress,
        src: RegisterAddress,
        immediate: i32,
    },
    Slli {
        dest: RegisterAddress,
        src: RegisterAddress,
        shamt: u8,
    },
    Srli {
        dest: RegisterAddress,
        src: RegisterAddress,
        shamt: u8,
    },
    Srai {
        dest: RegisterAddress,
        src: RegisterAddress,
        shamt: u8,
    },
    Add {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Sub {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Sll {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Slt {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Sltu {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Xor {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Srl {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Sra {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Or {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    And {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Fence {
        fm: u8,
        pi: bool,
        po: bool,
        pr: bool,
        pw: bool,
        si: bool,
        so: bool,
        sr: bool,
        sw: bool,
    },
    Ecall,
    Ebreak,
    Mul {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Mulh {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Mulhu {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Mulhsu {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Div {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Divu {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Rem {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },
    Remu {
        dest: RegisterAddress,
        src1: RegisterAddress,
        src2: RegisterAddress,
    },

    IllegalInstruction(u32),
}

#[derive(Copy, Clone, Debug)]
pub struct Instruction {
    decompressed_instruction: DecompressedInstruction,
    compressed: bool,
}

fn bit_range(value: u32, end: u32, start: u32) -> u32 {
    value >> start & ((1u64 << (end - start + 1)) - 1) as u32
}

fn i_immediate(value: u32) -> i32 {
    (value as i32) >> 20
}

fn s_immediate(value: u32) -> i32 {
    sign_extend(
        (bit_range(value, 11, 7) | (bit_range(value, 31, 25) << 5)) as i32,
        12,
    )
}

fn b_immediate(value: u32) -> i32 {
    sign_extend(
        ((bit_range(value, 11, 8) << 1)
            | (bit_range(value, 30, 25) << 5)
            | (bit_range(value, 7, 7) << 11)
            | (bit_range(value, 31, 31) << 12)) as i32,
        13,
    )
}
fn u_immediate(value: u32) -> i32 {
    (value & 0xFFFFF000) as i32
}

fn j_immediate(value: u32) -> i32 {
    sign_extend(
        ((bit_range(value, 30, 21) << 1)
            | (bit_range(value, 20, 20) << 11)
            | (bit_range(value, 19, 12) << 12)
            | (bit_range(value, 31, 31) << 20)) as i32,
        21,
    )
}

fn compressed_register(compressed_address: u8) -> RegisterAddress {
    compressed_address + 8
}

fn sign_extend(value: i32, width: u8) -> i32 {
    let shift = 32 - width;
    value << shift >> shift
}

impl From<u32> for Instruction {
    fn from(value: u32) -> Instruction {
        let opcode = value as u8 & OPCODE_MASK;
        let is_compressed = opcode & COMPRESSED_QUADRANT_MASK != 0b11;
        let rd = (value >> RD_SHIFT) as u8 & REGISTER_ADDRESS_MASK;
        let rs1 = (value >> RS1_SHIFT) as u8 & REGISTER_ADDRESS_MASK;
        let rs2 = (value >> RS2_SHIFT) as u8 & REGISTER_ADDRESS_MASK;
        let funct3 = (value >> FUNCT3_SHIFT) as u8 & FUNCT3_MASK;
        let funct7 = (value >> FUNCT7_SHIFT) as u8 & FUNCT7_MASK;
        let funct2 = bit_range(value, 11, 10);
        let compressed_value = value as u16;
        let compressed_funct3 = (value >> COMPRESSED_FUNCT3_SHIFT) as u8 & FUNCT3_MASK;
        let i_immediate = i_immediate(value);
        let s_immediate = s_immediate(value);
        let b_immediate = b_immediate(value);
        let u_immediate = u_immediate(value);
        let j_immediate = j_immediate(value);

        let decompressed_instruction = match (opcode, funct3, funct7) {
            (0b0110111, _, _) => DecompressedInstruction::Lui {
                dest: rd,
                immediate: u_immediate as u32,
            },
            (0b0010111, _, _) => DecompressedInstruction::Auipc {
                dest: rd,
                immediate: u_immediate,
            },
            (0b1101111, _, _) => DecompressedInstruction::Jal {
                dest: rd,
                offset: j_immediate,
            },
            (0b1100111, 0b000, _) => DecompressedInstruction::Jalr {
                dest: rd,
                base: rs1,
                offset: i_immediate,
            },
            (0b1100011, 0b000, _) => DecompressedInstruction::Beq {
                src1: rs1,
                src2: rs2,
                offset: b_immediate,
            },
            (0b1100011, 0b001, _) => DecompressedInstruction::Bne {
                src1: rs1,
                src2: rs2,
                offset: b_immediate,
            },
            (0b1100011, 0b100, _) => DecompressedInstruction::Blt {
                src1: rs1,
                src2: rs2,
                offset: b_immediate,
            },
            (0b1100011, 0b101, _) => DecompressedInstruction::Bge {
                src1: rs1,
                src2: rs2,
                offset: b_immediate,
            },
            (0b1100011, 0b110, _) => DecompressedInstruction::Bltu {
                src1: rs1,
                src2: rs2,
                offset: b_immediate,
            },
            (0b1100011, 0b111, _) => DecompressedInstruction::Bgeu {
                src1: rs1,
                src2: rs2,
                offset: b_immediate,
            },

            (0b0000011, 0b000, _) => DecompressedInstruction::Lb {
                dest: rd,
                base: rs1,
                offset: i_immediate,
            },
            (0b0000011, 0b001, _) => DecompressedInstruction::Lh {
                dest: rd,
                base: rs1,
                offset: i_immediate,
            },
            (0b0000011, 0b010, _) => DecompressedInstruction::Lw {
                dest: rd,
                base: rs1,
                offset: i_immediate,
            },
            (0b0000011, 0b100, _) => DecompressedInstruction::Lbu {
                dest: rd,
                base: rs1,
                offset: i_immediate,
            },
            (0b0000011, 0b101, _) => DecompressedInstruction::Lhu {
                dest: rd,
                base: rs1,
                offset: i_immediate,
            },
            (0b0100011, 0b000, _) => DecompressedInstruction::Sb {
                src: rs1,
                base: rs2,
                offset: s_immediate,
            },
            (0b0100011, 0b001, _) => DecompressedInstruction::Sh {
                src: rs1,
                base: rs2,
                offset: s_immediate,
            },
            (0b0100011, 0b010, _) => DecompressedInstruction::Sw {
                src: rs1,
                base: rs2,
                offset: s_immediate,
            },
            (0b0010011, 0b000, _) => DecompressedInstruction::Addi {
                dest: rd,
                src: rs1,
                immediate: i_immediate,
            },
            (0b0010011, 0b010, _) => DecompressedInstruction::Slti {
                dest: rd,
                src: rs1,
                immediate: i_immediate,
            },
            (0b0010011, 0b011, _) => DecompressedInstruction::Sltiu {
                dest: rd,
                src: rs1,
                immediate: i_immediate,
            },
            (0b0010011, 0b100, _) => DecompressedInstruction::Xori {
                dest: rd,
                src: rs1,
                immediate: i_immediate,
            },
            (0b0010011, 0b110, _) => DecompressedInstruction::Ori {
                dest: rd,
                src: rs1,
                immediate: i_immediate,
            },
            (0b0010011, 0b111, _) => DecompressedInstruction::Andi {
                dest: rd,
                src: rs1,
                immediate: i_immediate,
            },
            (0b0010011, 0b001, 0b0000000) => DecompressedInstruction::Slli {
                dest: rd,
                src: rs1,
                shamt: i_immediate as u8 & SHAMT_MASK,
            },
            (0b0010011, 0b101, 0b0000000) => DecompressedInstruction::Srli {
                dest: rd,
                src: rs1,
                shamt: i_immediate as u8 & SHAMT_MASK,
            },
            (0b0010011, 0b101, 0b0100000) => DecompressedInstruction::Srai {
                dest: rd,
                src: rs1,
                shamt: i_immediate as u8 & SHAMT_MASK,
            },
            (0b0110011, 0b000, 0b0000000) => DecompressedInstruction::Add {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b000, 0b0100000) => DecompressedInstruction::Sub {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b001, 0b0000000) => DecompressedInstruction::Sll {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b010, 0b0000000) => DecompressedInstruction::Slt {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b011, 0b0000000) => DecompressedInstruction::Sltu {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b100, 0b0000000) => DecompressedInstruction::Xor {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b101, 0b0000000) => DecompressedInstruction::Srl {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b101, 0b0100000) => DecompressedInstruction::Sra {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b110, 0b0000000) => DecompressedInstruction::Or {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b111, 0b0000000) => DecompressedInstruction::And {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0001111, 0b000, _) => DecompressedInstruction::Fence {
                fm: (value >> 28) as u8,
                pi: ((value >> 27) & 0x1) != 0,
                po: ((value >> 26) & 0x1) != 0,
                pr: ((value >> 25) & 0x1) != 0,
                pw: ((value >> 24) & 0x1) != 0,
                si: ((value >> 23) & 0x1) != 0,
                so: ((value >> 22) & 0x1) != 0,
                sr: ((value >> 21) & 0x1) != 0,
                sw: ((value >> 20) & 0x1) != 0,
            },
            (0b0110011, 0b000, 0b0000001) => DecompressedInstruction::Mul {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b001, 0b0000001) => DecompressedInstruction::Mulh {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b010, 0b0000001) => DecompressedInstruction::Mulhsu {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b011, 0b0000001) => DecompressedInstruction::Mulhu {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b100, 0b0000001) => DecompressedInstruction::Div {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b101, 0b0000001) => DecompressedInstruction::Divu {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b110, 0b0000001) => DecompressedInstruction::Rem {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (0b0110011, 0b111, 0b0000001) => DecompressedInstruction::Remu {
                dest: rd,
                src1: rs1,
                src2: rs2,
            },
            (_, _, _) if value == 0b00000000000000000000000001110011 => {
                DecompressedInstruction::Ecall
            }
            (_, _, _) if value == 0b00000000000100000000000001110011 => {
                DecompressedInstruction::Ebreak
            }
            (_, _, _) if opcode & COMPRESSED_QUADRANT_MASK == 0b00 => match compressed_funct3 {
                0b000 => {
                    let imm = (bit_range(value, 5, 5) << 3)
                        | (bit_range(value, 6, 6) << 2)
                        | (bit_range(value, 10, 7) << 6)
                        | (bit_range(value, 12, 11) << 4);
                    let rd = compressed_register(bit_range(value, 4, 2) as u8);

                    if imm == 0 {
                        DecompressedInstruction::IllegalInstruction(compressed_value as u32)
                    } else {
                        DecompressedInstruction::Addi {
                            dest: rd,
                            src: 2,
                            immediate: imm as i32,
                        }
                    }
                }
                0b010 | 110 => {
                    let rd = compressed_register(bit_range(value, 4, 2) as u8);
                    let rs1 = compressed_register(bit_range(value, 9, 7) as u8);
                    let imm = bit_range(value, 5, 5) << 6
                        | bit_range(value, 6, 6) << 2
                        | bit_range(value, 12, 10) << 3;
                    if compressed_funct3 == 0b010 {
                        DecompressedInstruction::Lw {
                            dest: rd,
                            base: rs1,
                            offset: imm as i32,
                        }
                    } else {
                        DecompressedInstruction::Sw {
                            src: rd,
                            base: rs1,
                            offset: imm as i32,
                        }
                    }
                }
                _ => DecompressedInstruction::IllegalInstruction(compressed_value as u32),
            },
            (_, _, _) if opcode & COMPRESSED_QUADRANT_MASK == 0b01 => match compressed_funct3 {
                0b000 | 0b010 => {
                    let rd = bit_range(value, 11, 7) as RegisterAddress;
                    let imm = bit_range(value, 6, 2) | (bit_range(value, 12, 12) << 5);
                    let imm = sign_extend(imm as i32, 6);
                    let src = if compressed_funct3 == 0b000 { rd } else { 0 };
                    DecompressedInstruction::Addi {
                        dest: rd,
                        src,
                        immediate: imm,
                    }
                }
                0b001 | 0b101 => {
                    let imm = (bit_range(value, 12, 12) << 11)
                        | (bit_range(value, 11, 11) << 4)
                        | (bit_range(value, 10, 9) << 8)
                        | (bit_range(value, 8, 8) << 10)
                        | (bit_range(value, 7, 7) << 6)
                        | (bit_range(value, 6, 6) << 7)
                        | (bit_range(value, 5, 3) << 1)
                        | (bit_range(value, 2, 2) << 5);

                    let imm = (imm as i32) << 22 >> 22;

                    let dest = if compressed_funct3 == 0b101 { 0 } else { 1 };
                    DecompressedInstruction::Jal { dest, offset: imm }
                }
                0b011 => {
                    let rd = bit_range(value, 11, 7) as RegisterAddress;
                    if rd == 2 {
                        let imm = (bit_range(value, 2, 2) << 5)
                            | (bit_range(value, 4, 3) << 7)
                            | (bit_range(value, 5, 5) << 6)
                            | (bit_range(value, 6, 6) << 4)
                            | (bit_range(value, 12, 12) << 9);
                        DecompressedInstruction::Addi {
                            dest: rd,
                            src: 2,
                            immediate: imm as i32,
                        }
                    } else {
                        let imm = (bit_range(value, 6, 2) << 12) | (bit_range(value, 12, 12) << 17);
                        DecompressedInstruction::Lui {
                            dest: rd,
                            immediate: imm,
                        }
                    }
                }
                0b100 => {
                    let rd = compressed_register(bit_range(value, 9, 7) as u8);
                    match funct2 {
                        0b00 if bit_range(value, 12, 12) == 0 => DecompressedInstruction::Srli {
                            dest: rd,
                            src: rd,
                            shamt: bit_range(value, 6, 2) as u8,
                        },
                        0b10 => {
                            let imm = bit_range(value, 6, 2) | bit_range(value, 12, 12) << 5;
                            let imm = sign_extend(imm as i32, 6);
                            DecompressedInstruction::Andi {
                                dest: rd,
                                src: rd,
                                immediate: imm,
                            }
                        }
                        0b11 => {
                            let rs2 = compressed_register(bit_range(value, 4, 2) as u8);
                            match (bit_range(value, 12, 12), bit_range(value, 6, 5)) {
                                (0, 0b00) => DecompressedInstruction::Sub {
                                    dest: rd,
                                    src1: rd,
                                    src2: rs2,
                                },
                                (0, 0b01) => DecompressedInstruction::Xor {
                                    dest: rd,
                                    src1: rd,
                                    src2: rs2,
                                },
                                (0, 0b10) => DecompressedInstruction::Or {
                                    dest: rd,
                                    src1: rd,
                                    src2: rs2,
                                },
                                (0, 0b11) => DecompressedInstruction::And {
                                    dest: rd,
                                    src1: rd,
                                    src2: rs2,
                                },
                                _ => DecompressedInstruction::IllegalInstruction(
                                    compressed_value as u32,
                                ),
                            }
                        }
                        _ => DecompressedInstruction::IllegalInstruction(compressed_value as u32),
                    }
                }
                0b110 | 0b111 => {
                    let rs1 = compressed_register(bit_range(value, 9, 7) as u8);
                    let imm = (bit_range(value, 2, 2) << 5)
                        | (bit_range(value, 4, 3) << 1)
                        | (bit_range(value, 6, 5) << 6)
                        | (bit_range(value, 11, 10) << 3)
                        | (bit_range(value, 12, 12) << 8);
                    let imm = sign_extend(imm as i32, 9);
                    if compressed_funct3 == 0b110 {
                        DecompressedInstruction::Beq {
                            src1: rs1,
                            src2: 0,
                            offset: imm,
                        }
                    } else {
                        DecompressedInstruction::Bne {
                            src1: rs1,
                            src2: 0,
                            offset: imm,
                        }
                    }
                }
                _ => DecompressedInstruction::IllegalInstruction(compressed_value as u32),
            },
            (_, _, _) if opcode & COMPRESSED_QUADRANT_MASK == 0b10 => match compressed_funct3 {
                0b000 => {
                    let imm = bit_range(value, 6, 2);
                    if bit_range(value, 12, 12) == 0 {
                        DecompressedInstruction::Slli {
                            dest: rd,
                            src: rd,
                            shamt: imm as u8,
                        }
                    } else {
                        DecompressedInstruction::IllegalInstruction(compressed_value as u32)
                    }
                }
                0b010 => {
                    let imm = (bit_range(value, 3, 2) << 6)
                        | (bit_range(value, 6, 2) << 2)
                        | (bit_range(value, 12, 12) << 5);
                    if rd != 0 {
                        DecompressedInstruction::Lw {
                            dest: rd,
                            base: 2,
                            offset: imm as i32,
                        }
                    } else {
                        DecompressedInstruction::IllegalInstruction(compressed_value as u32)
                    }
                }
                0b100 => {
                    let rs2 = bit_range(value, 6, 2) as RegisterAddress;
                    match (bit_range(value, 12, 12), rd, rs2) {
                        (0, _, 0) => {
                            if rd == 0 {
                                DecompressedInstruction::IllegalInstruction(compressed_value as u32)
                            } else {
                                DecompressedInstruction::Jalr {
                                    dest: 0,
                                    base: rd,
                                    offset: 0,
                                }
                            }
                        }
                        (0, _, _) => {
                            if rd == 0 {
                                DecompressedInstruction::IllegalInstruction(compressed_value as u32)
                            } else {
                                DecompressedInstruction::Add {
                                    dest: rd,
                                    src1: 0,
                                    src2: rs2,
                                }
                            }
                        }
                        (1, 0, 0) => DecompressedInstruction::Ebreak,
                        (1, _, 0) => DecompressedInstruction::Jalr {
                            dest: 1,
                            base: rd,
                            offset: 0,
                        },
                        (1, _, _) => DecompressedInstruction::Add {
                            dest: rd,
                            src1: rd,
                            src2: rs2,
                        }, // C.ADD
                        _ => DecompressedInstruction::IllegalInstruction(compressed_value as u32),
                    }
                }
                0b110 => {
                    let rs2 = bit_range(value, 6, 2) as RegisterAddress;
                    let imm = (bit_range(value, 8, 7) << 6) | (bit_range(value, 12, 9) << 2);
                    DecompressedInstruction::Sw {
                        src: rs2,
                        base: 2,
                        offset: imm as i32,
                    }
                }
                _ => DecompressedInstruction::IllegalInstruction(compressed_value as u32),
            },
            _ => DecompressedInstruction::IllegalInstruction(value),
        };
        Instruction {
            decompressed_instruction,
            compressed: is_compressed,
        }
    }
}

impl From<[u8; 4]> for Instruction {
    fn from(value: [u8; 4]) -> Instruction {
        Instruction::from(u32::from_le_bytes(value))
    }
}

#[cfg(test)]
mod tests {
    use super::bit_range;
    use super::s_immediate;

    #[test]
    fn bit_range_works() {
        let value = 0b11001011001010111100010111101010;
        assert_eq!(bit_range(value, 31, 0), 0b11001011001010111100010111101010);
        assert_eq!(bit_range(value, 0, 0), 0b0);
        assert_eq!(bit_range(value, 1, 1), 0b1);
        assert_eq!(bit_range(value, 1, 0), 0b10);
        assert_eq!(bit_range(value, 31, 29), 0b110);
        assert_eq!(bit_range(value, 29, 20), 0b010110010);
    }

    #[test]
    fn s_immediate_works() {
        assert_eq!(
            s_immediate((0b0101101 << 25) | (0b11001 << 7)),
            0b010110111001
        );
        assert_eq!(
            s_immediate((0b1101101 << 25) | (0b11001 << 7)),
            0b11111111111111111111110110111001u32 as i32
        );
    }
}
