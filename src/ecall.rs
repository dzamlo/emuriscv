use std::collections::HashMap;
use std::ffi::OsString;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::stderr;
use std::io::stdin;
use std::io::stdout;
use std::io::Read;
use std::io::Write;
use std::os::unix::prelude::OpenOptionsExt;
use std::os::unix::prelude::OsStringExt;
use std::path::PathBuf;
use std::process::exit;

use crate::address::Address;
use crate::bus::Bus;
use anyhow::bail;
use anyhow::Result;
use rand::rngs::OsRng;
use rand::RngCore;

pub const ARGS_LEN: usize = 7;

const SYSCALL_NR_EXIT: u32 = 93;
const SYSCALL_NR_READ: u32 = 63;
const SYSCALL_NR_WRITE: u32 = 64;
const SYSCALL_NR_OPEN: u32 = 512;
const SYSCALL_NR_RNG: u32 = 1024;

const ACCMODE_MASK: u32 = 3;

pub trait EcallHandler {
    fn handle_ecall<B: Bus>(
        &mut self,
        syscall_nr: u32,
        args: [u32; ARGS_LEN],
        bus: &mut B,
        debug: bool,
    ) -> Result<u32>;
}

pub struct DefaultEcallHandler {
    fds: HashMap<u32, File>,
    next_fd: u32,
    os_rng: OsRng,
}

impl DefaultEcallHandler {
    pub fn new() -> DefaultEcallHandler {
        DefaultEcallHandler {
            fds: HashMap::new(),
            next_fd: 4,
            os_rng: OsRng::default(),
        }
    }
}

impl EcallHandler for DefaultEcallHandler {
    fn handle_ecall<B: Bus>(
        &mut self,
        syscall_nr: u32,
        args: [u32; ARGS_LEN],
        bus: &mut B,
        debug: bool,
    ) -> Result<u32> {
        if debug {
            eprintln!("Calling ecall with syscall number {syscall_nr} and args {args:?}")
        }
        let return_code = match syscall_nr {
            SYSCALL_NR_WRITE => {
                let fd = args[0];
                let address: Address = args[1].into();
                let count = args[2];
                if debug {
                    eprintln!(
                        "Calling write to fd {fd} from address {address:?} and count {count}"
                    );
                }

                let mut f: Box<dyn Write> = match fd {
                    1 => Box::new(stdout()),
                    2 => Box::new(stderr()),
                    _ => match self.fds.get(&fd) {
                        Some(file) => Box::new(file),
                        None => {
                            if debug {
                                eprintln!("Invalid fd ({fd})");
                            }
                            return Ok(-1i32 as u32);
                        }
                    },
                };

                let mut result = count;
                for i in 0..count {
                    if f.write_all(&[bus.read_u8(address + i, debug)?]).is_err() {
                        result = -1i32 as u32;
                        break;
                    }
                }
                result
            }
            SYSCALL_NR_READ => {
                let fd = args[0];
                let address: Address = args[1].into();
                let count = args[2];

                let mut f: Box<dyn Read> = match fd {
                    0 => Box::new(stdin()),
                    _ => match self.fds.get(&fd) {
                        Some(file) => Box::new(file),
                        None => {
                            if debug {
                                eprintln!("Invalid fd ({fd})");
                            }
                            return Ok(-1i32 as u32);
                        }
                    },
                };

                let mut result = count;

                for i in 0..count {
                    let mut byte = [0u8; 1];
                    match f.read(&mut byte) {
                        Ok(0) => {
                            result = i;
                            break;
                        }
                        Ok(_) => {}
                        Err(_) => {
                            result = -1i32 as u32;
                            break;
                        }
                    }

                    bus.write_u8(address + i, byte[0], debug)?;
                }

                result
            }
            SYSCALL_NR_OPEN => {
                let fd = self.next_fd;
                self.next_fd += 1;

                let mut address = args[0];
                let flags = args[1];
                let mut path = Vec::new();
                loop {
                    let byte = bus.read_u8(address.into(), debug)?;
                    if byte == 0 {
                        break;
                    }
                    path.push(byte);

                    address += 1;
                }
                let path = PathBuf::from(OsString::from_vec(path));

                let (read, write) = match flags & ACCMODE_MASK {
                    0b00 => (true, false),
                    0b01 => (false, true),
                    _ => (true, true),
                };

                if debug {
                    eprintln!("Opening {path:?}, with read={read}, write={write}, flags={flags}");
                }
                match OpenOptions::new()
                    .read(read)
                    .write(write)
                    .custom_flags(flags as i32)
                    .open(path)
                {
                    Ok(file) => {
                        self.fds.insert(fd, file);
                        fd
                    }
                    Err(_) => -1i32 as u32,
                }
            }
            SYSCALL_NR_EXIT => {
                let exit_code = args[0] as i32;
                if debug {
                    eprintln!("Exiting the process with exit code {exit_code}");
                }
                exit(exit_code)
            }
            SYSCALL_NR_RNG => self.os_rng.next_u32(),
            _ => bail!("unknown syscall number ({syscall_nr})"),
        };

        Ok(return_code)
    }
}
