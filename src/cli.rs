use clap::Parser;
use clap_num::maybe_hex;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Executable to run, default to stdin
    #[arg(short, long)]
    pub executable: Option<PathBuf>,

    /// Enable debug output
    #[arg(long)]
    pub debug: bool,

    /// Start at another address than 0
    #[arg(long, value_parser=maybe_hex::<u32>, default_value_t = 0)]
    pub start_pc: u32,
}
