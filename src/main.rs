use address::Address;
use anyhow::Context;
use anyhow::Result;
use bus::Bus;
use clap::Parser;
use cpu::Cpu;
use cpu::Instruction;
use ecall::DefaultEcallHandler;
use memory::Memory;
use std::fs::File;
use std::io::stdin;

use crate::cli::Args;

mod address;
mod bus;
mod cli;
mod cpu;
mod ecall;
mod memory;

fn main() -> Result<()> {
    let args = Args::parse();

    let mut main_memory = Memory::new();
    let mut ecall_handler = DefaultEcallHandler::new();
    let mut cpu = Cpu::new();
    cpu.jump(args.start_pc.into(), args.debug)?;

    match args.executable {
        Some(path) => {
            let f = File::open(&path).context(format!("Cannot open the executable {path:?}"))?;
            main_memory.load_file(Address::ZERO, f, args.debug)?;
        }
        None => main_memory.load_file(Address::ZERO, stdin(), args.debug)?,
    };

    loop {
        let raw_instruction = main_memory.read_u32(cpu.get_pc(), args.debug)?;
        let instruction = Instruction::from(raw_instruction);
        cpu.execute(
            instruction,
            &mut main_memory,
            &mut ecall_handler,
            args.debug,
        )?;
    }
}
