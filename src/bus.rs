use crate::address::Address;
use anyhow::Result;

pub trait Bus {
    fn read_u8(&mut self, address: Address, debug: bool) -> Result<u8>;
    fn read_u16(&mut self, address: Address, debug: bool) -> Result<u16>;
    fn read_u32(&mut self, address: Address, debug: bool) -> Result<u32>;
    fn write_u8(&mut self, address: Address, value: u8, debug: bool) -> Result<()>;
    fn write_u16(&mut self, address: Address, value: u16, debug: bool) -> Result<()>;
    fn write_u32(&mut self, address: Address, value: u32, debug: bool) -> Result<()>;
}
