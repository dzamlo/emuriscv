use std::collections::HashMap;
use std::io::Read;
use std::ops::Index;
use std::ops::IndexMut;

use anyhow::bail;

use crate::address::Address;
use crate::bus::Bus;

const PAGE_SIZE: u32 = 1 << 12;
const PAGE_SHIFT: u32 = PAGE_SIZE.ilog2();
const PAGE_MASK: u32 = PAGE_SIZE - 1;

#[derive(Clone, Debug)]
struct Page([u8; PAGE_SIZE as usize]);

impl Index<Address> for Page {
    type Output = u8;

    fn index(&self, index: Address) -> &Self::Output {
        &self.0[index.0 as usize]
    }
}
impl IndexMut<Address> for Page {
    fn index_mut(&mut self, index: Address) -> &mut Self::Output {
        &mut self.0[index.0 as usize]
    }
}

impl Page {
    fn new() -> Page {
        Page([0; PAGE_SIZE as usize])
    }

    fn read_u8(&self, address: Address) -> anyhow::Result<u8> {
        if address >= PAGE_SIZE {
            bail!("address larger than a page ({address:?})")
        }

        Ok(self[address])
    }

    fn write_u8(&mut self, address: Address, value: u8) -> anyhow::Result<()> {
        if address >= PAGE_SIZE {
            bail!("address larger than a page ({:?})", address)
        }
        self[address] = value;
        Ok(())
    }

    fn write_u16(&mut self, address: Address, value: u16) -> anyhow::Result<()> {
        if (address & 0b1) != 0 {
            bail!("address not aligned ({address:?})")
        }
        let bytes = value.to_le_bytes();
        for i in 0..2 {
            self.write_u8(address + i, bytes[i as usize])?;
        }
        Ok(())
    }

    fn write_u32(&mut self, address: Address, value: u32) -> anyhow::Result<()> {
        if (address & 0b11) != 0 {
            bail!("address not aligned ({address:?})")
        }
        let bytes = value.to_le_bytes();
        for i in 0..4 {
            self.write_u8(address + i, bytes[i as usize])?;
        }
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct Memory {
    pages: HashMap<Address, Page>,
}

impl Memory {
    pub fn new() -> Memory {
        Memory {
            pages: HashMap::new(),
        }
    }

    fn get_mut_page(&mut self, address: Address, debug: bool) -> &mut Page {
        let page_address = address >> PAGE_SHIFT;
        self.pages.entry(page_address).or_insert_with(|| {
            if debug {
                eprintln!("Creating new page at address {page_address:?}")
            };
            Page::new()
        })
    }

    pub fn load_file<R: Read>(
        &mut self,
        base_address: Address,
        mut f: R,
        debug: bool,
    ) -> anyhow::Result<()> {
        let mut byte: [u8; 1] = [0];
        let mut i = 0;
        loop {
            let read = f.read(&mut byte[..])?;
            if read == 0 {
                break;
            }
            self.write_u8(base_address + i, byte[0], debug)?;

            i += 1;
        }
        Ok(())
    }
}

impl Bus for Memory {
    fn read_u8(&mut self, address: Address, debug: bool) -> anyhow::Result<u8> {
        if debug {
            eprint!("Reading 8 bits at address {address:?}, ");
        }
        let page_address = address >> PAGE_SHIFT;
        let adress_in_page = address & PAGE_MASK;

        let page = self.pages.get(&page_address);
        let value = match page {
            Some(page) => page.read_u8(adress_in_page),
            None => {
                if debug {
                    eprint!("page doesn't exist, ")
                }
                Ok(0)
            }
        }?;
        if debug {
            eprintln!("value={value}");
        }
        Ok(value)
    }

    fn read_u16(&mut self, address: Address, debug: bool) -> anyhow::Result<u16> {
        if debug {
            eprint!("Reading 16 bits at address {address:?}, ");
        }
        let value =
            self.read_u8(address, false)? as u16 | (self.read_u8(address + 1, false)? as u16) << 8;

        if debug {
            eprintln!("value={value}");
        }
        Ok(value)
    }

    fn read_u32(&mut self, address: Address, debug: bool) -> anyhow::Result<u32> {
        if debug {
            eprint!("Reading 32 bits at address {address:?}, ");
        }

        let value = self.read_u8(address, false)? as u32
            | (self.read_u8(address + 1, false)? as u32) << 8
            | (self.read_u8(address + 2, false)? as u32) << 16
            | (self.read_u8(address + 3, false)? as u32) << 24;

        if debug {
            eprintln!("value={value}");
        }
        Ok(value)
    }

    fn write_u8(&mut self, address: Address, value: u8, debug: bool) -> anyhow::Result<()> {
        if debug {
            eprintln!("Writing 8 bits at address {address:?} (value={value})");
        }
        let page = self.get_mut_page(address, debug);
        page.write_u8(address & PAGE_MASK, value)
    }

    fn write_u16(&mut self, address: Address, value: u16, debug: bool) -> anyhow::Result<()> {
        if debug {
            eprintln!("Writing 16 bits at address {address:?} (value={value})");
        }
        let page = self.get_mut_page(address, debug);
        page.write_u16(address & PAGE_MASK, value)
    }

    fn write_u32(&mut self, address: Address, value: u32, debug: bool) -> anyhow::Result<()> {
        if debug {
            eprintln!("Writing 32 bits at address {address:?} (value={value})");
        }
        let page = self.get_mut_page(address, debug);
        page.write_u32(address & PAGE_MASK, value)
    }
}
